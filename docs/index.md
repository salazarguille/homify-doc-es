# Bienvenido a Homify by Senzil S.R.L.

Le damos la bienvenida a la documentacion funcional de Homify. Aqui podrás encontrar toda la información oficial del sistema de gestion de mantenimiento de Senzil S.R.L., llamado MMS by Senzil. Si todavia no conoces a MMS, a continuacion te mostramos mas detalles.

## Que es Homify?


**Homify** es un sistema de gestion de mantenimiento pensado para todas aquellas empresas que requieren un salto de calidad en su gestion, en conjunto con un agregado de valor en el servicio a sus clientes. MMS por Senzil S.R.L. se basa en sus cuatro pilares fundamentales para cumplir lo que promete a sus clientes, haciendo que el negocio de sus clientes llegue al maximo de su ideal.

## Modulos de Homify


### Ordenes de Trabajo

Gestion y administracion de ordenes de trabajo para empleados, permitiendo registrar cualquier trabajo realizado.

### Gestion de Usuario

Centralizacion de la gestion y adminitracion de los usuarios o empleados de la empresa.

### Gestion de Permisos / Roles
	
Configuracion mediante permisos y roles para la visualizacion y ejecucion de funciones de la aplicacion.

### Aplicacion Movil

Innovacion tecnologica de la mano de una aplicacion movil que permite utilizar la aplicacion MMS desde un telefono celular con Android.

### Gestion de Ausencia de Personal
	
Registracion de las ausencias del personal, permitiendo mejorar la gestion de ausencias, y aumentar la visibilidad de sus indices.

### Gestion de Stock de Productos

Administracion y gestion del stock y los productos utilizados por la empresa, permitiendo visualizar monitores con los indices de cada producto.

### Funciones Exclusivas

Utilizacion y configuracion de funciones exclusivas solamente para su empresa, permitiendo obtener una ventaja competitiva con su competencia.

### Estrategias de Estimacion de Tareas

Posiblidad de aplicar diferentes estrategias de estimacion para las ordenes de trabajo asignadas a sus empleados.

### Gestion de Documentos Rutinarios
	
Gestion y administracion de documentos para tareas rutinarias, con posibildad de exportacion en formato PDF para su impresion.

### Generacion de tareas rutinarias

A partir de rutinas asociadas a un documento de rutina, se puede generar ordenes de trabajo rutinarias con una frecuencia definida.

### Notificaciones

Configuracion de notificaciones en las acciones aplicadas a las ordenes de trabajo.

### Mas
	
Muchos otros modulos totalmente configurables, y extensibles dependiendo de las necesidades de nuestros clientes.


## Funcionalidades

   
### Gestion de Ordenes de Mantenimiento

*   Busqueda de las ordenes de trabajo por distintos criterios.
*   Listado de las ordenes de trabajo.
*	Acciones de creacion, estimacion, asignacion, cancelacion, pausar, cargar horas de trabajo, sobre las tareas.
*	Administracion de ordenes de trabajo para todos sus empleados.
*	Identificacion de retrasos en los desarrollos de las ordenes de trabajo.
*	Monitor de diferentes indices relacionados con las ordenes de trabajo.

### Gestion de Usuarios

*	Visualizacion de los perfiles de cada uno de los usuarios
*	Administracion de los usuarios de todos sus empleados.
*	Posibilidad de habilitar/deshabilitar el acceso de usuarios.

### Permisos y Roles

*	Menu dinamico generado a partir de los permisos y roles del usuario.
*	Configuracion de permisos y roles de los usuarios de la empresa para cada accion sobre sus datos.
*	Visualizacion de monitores con indices por parte de los administradores de la empresa.

### Ausencias de Personal

*	Visualizacion de distintos indices sobre las ausencias de sus empleados.
*	Configuracion de dias laborables de la empresa, y de las ausencias de sus empleados.
*	Gestion y control sobre las ausencias de usuario.
 
### Aplicacion Movil

*	Visualizacion de calendario personal con todas las ordenes de trabajos asignadas.
*	Aplicacion movil totalmente desarrollada en el sistema operativo mas utilizado, Android.
*	Creacion de ordenes de trabajo para mantenimiento de forma casi instantanea.
*	Visualizacion de su perfil de usuario.
*	Carga de horas de trabajo sobre las ordenes de trabajo.

### Rutinas y Documentos Rutinarios

*	Exportacion de documentos en formato PDF para proveedores terciarizados.
*	Generacion de tareas rutinarias con distinas frecuencias a partir de las rutinas.
*	Basado para tareas de mantenimiento rutinarias.
*	Administracion y gestion de documentos rutinarios.

### Funcionalidades Exclusivas

*	Flexible configuracion de las funciones a partir de los administradores de la aplicacion.
*	Definicion temporal de las funciones exclusivas.
*	Configuracion de funciones exclusivas para su empresa.

### Notificaciones

*	Motor de notificaciones basado en las ordenes de trabajo, y rutinas.
*	Flexible generacion de notificaciones a partir de acciones aplicadas a las ordenes de trabajo.
*	Notificaciones enviadas por email y/u otras aplicaciones a partir de servicios web.

