# Informacion de Versiones

A continuacion mostraremos las funcionalidades, modificaciones, y correcciones agregadas durante cada version lanzada por **MMS**.

## Version 1.7.0.RELEASE

Las nuevas funcionalidades incluidas son:

*	Descargar la aplicacion movil desde la aplicacion web.
*	Documentacion online para servicios REST.
*	Aplicacion movil para sistemas Android.

Se corrigen las siguientes funcionalidades:

*	Servicios REST para ordenes de trabajo.
*	Logica de importacion de datos.
*	Permisos y roles para acceder a diversas funcionalidades.
 
Se realizan mejoras:

*	Mejoras tecnicas para futuros desarrollos.
*	Mejoras de diseNNo a nivel tecnico.

## Version 1.6.5.RELEASE

Version inicial de MMS. Las funcionalidades incluidas son: 

*	Administracion de ordenes de trabajo.
*	Administracion de usuarios.
*	Administracion de documentos rutinarios.
*	Administracion de rutinas.
*	Administracion de ausencia de personal.
*	Monitores para diversas funcionalidades.
*	Servicios REST para aplicacion Android (no incluida en esta version).
