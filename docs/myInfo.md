# Mis Datos

A continuacion se detalla como visualizar la informacion del usuario logueado.

## Mi Perfil

Se puede visualizar los datos del usuario logueado de la siguiente manera:

* Acceder al menu superior derecho llamado "_Mi Cuenta_ ".
* Hacer click en la opcion "_Perfil de Usuario_". 

## Mi Empresa

Asi mismo se puede visualizar los datos sobre la empresa del usuario logueado haciendo:

* Acceder al menu superior derecho llamado "_Mi Cuenta_ ".
* Hacer click en la opcion "_Mi CompaNNia_".

## Descargar Aplicacion Movil

Como usuario de _MMS_, en caso de haber solicitado el permiso para utilizar la aplicacion movil, a la misma se puede acceder de la siguiente manera:  

* Acceder al menu superior derecho llamado "_Mi Cuenta_ ".
* Hacer click en la opcion "_Descargar App._".

## Cambiar ContraseNNa

Si usted desea modificar su contraseNNa, debe realizar los siguientes pasos:

* Acceder a su perfil de usuario (Ver [Perfil de Usuario](myInfo.md#mi-perfil "Perfil de usuario")).
* Hacer click en el boton _Cambiar ContraseNNa_.
* Completar el formulario para realizar el cambio de contraseNNa.  


## Salir de **MMS** 

Para desloguearse de **MMS**, se deben realizar los siguientes pasos:  

* Acceder al menu superior derecho llamado "_Mi Cuenta_ ".
* Hacer click en la opcion "_Salir_".
 