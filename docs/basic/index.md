# Administracion de Ausencia de Personal

**MMS** ofrece la posibilidad de administrar las ausencias del personal de la empresa.

## Listado de Ausencias de Usuarios.

Para acceder al listado de las ausencias de los usuarios de la empresa, debe:

*	Acceder al menu principal llamado _Admin Ausencias_.
*	Seleccionar la opcion _Listado Ausencias de Usuarios_.

En el listado se pueden ver las ausencias de cada usuario de la siguiente manera:

*	Seleccionar el turno.
*	Seleccionar el usuario a quien desea ver sus ausencias.
*	Presionar el boton _Actualizar_.

Una vez presionado el boton, se actualizara el listado ubicado debajo. En ese listado se pueden ver las fechas de las ausencias, y el motivo. Asi tambien por cada ausencia registrada se puede ver los detalles de los horarios, y su descripcion.

## Monitor de Ausencias

Para acceder al monitor de ausencias, debe:

*	Acceder al menu principal llamado _Admin Ausencias_.
*	Seleccionar la opcion _Monitor de Ausencias_.

## Crear Ausencia de Usuario

Para crear una ausencia a un usuario de la empresa, debe:

*	Acceder al Listado de Ausencias (ver [Listado de Ausencias de Usuarios](../admin/absence.md#listado-de-ausencias-de-usuarios "Ver Listado de Ausencia de Usuarios")).
*	Seleccionar el usuario a quien se desea crear una ausencia.
*	Presionar el boton "_Crear Ausencia_".
*	Completar el formulario, agregando al menos un rango de ausencia para la fecha seleccionada en el formulario.

Una vez completado el formulario, se debe presionar el boton _Crear Ausencia_.

## Dias No Laborables

Para acceder al listado de dias no laborables, debe:

*	Acceder al menu principal llamado _Admin Ausencias_.
*	Seleccionar la opcion _Dias No Laborables_.

Luego debe seleccionar el turno, y presionar el boton _Actualizar_, y se refrescara el listado de dias no laborables de la empresa.

### Configuracion de Dias No Laborales.

Para configurar los dias no laborables de la empresa, debe:

*	Acceder a los Dias No Laborables (ver [Dias No Laborables](../admin/absence.md#dias-no-laborables "Ver Dias No Laborables")).
*	Presionar el boton _Agregar Dia NO Laborable_.
*	Completar el formulario con la informacion necesaria, agregando los rangos de horario.

Luego presionar el boton _Guardar_.