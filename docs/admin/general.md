# Administracion General

**MMS** facilita la utilizacion de la aplicacion utilizando una estructura definida en la mayoria de sus funcionalidades. De este modo permite a sus usuarios, una manera mas facil de operar con el sistema.

## Listados

En todos los listados ofrecidos en las funcionalidades de **MMS**, se ofrecen las siguientes caracteristicas: 

### Busquedas Dinamica

En cada listado, se ofrece una busqueda dinamica por cada campo visualizado, ubicado sobre el listado. Al ir ingresando los terminos de busqueda, el listado se actualiza automaticamente. 

### Acciones Disponibles

Por default, en todos los listado de ***MMS** se ofrecen las siguientes acciones para cada registro:

*   Editar el registro.
*   Eliminar el registro.
 
`Nota: Cada una de estas acciones estaran habilitadas en caso que el usuario posea los permisos necesarios.` 

#### Edicion de Item

Cada registro en el listado puede ser modificado, donde se puede editar cada uno de los campos. Para realizarlo, se debe hacer click en el icono _Libro con Lapiz_.

#### Eliminar un Item

Asimismo como en la edicion, cada registro del listado puede ser eliminado, haciendo click en el boton _X_. Luego la aplicacion le pedira una confirmacion de la accion.

### Crear un Item

Ademas se puede crear un item para agregar en el listado. Para la creacion se debe hacer click en el boton _Crear Item_ debajo del listado.

`Nota: Esta accion esta disponible solamente si posee los permisos necesarios.` 