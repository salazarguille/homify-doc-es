# Administracion de Documentos Rutinarios

En esta seccion se detallan las funcionalidades asociadas a los documentos rutinarios. Estos son utilizados para definir aquellas tareas que son rutinarias, y las cuales tienen pasos a ser cumplidos para finalizar una tarea de forma correcta.


## Listado de Documentos Rutinarios

En el listado de documentos se pueden visualizar todos los documentos de la empresa, como asi tambien:

*	Su creador.
*	Su aprobador.
*	Cantidad de actividades que componen el documento.

## Acciones Disponibles

### Generacion de PDF

En cada documento se puede exportar a PDF. En el mismo se agregara ademas de las actividades asociadas al documento, los datos del mismo, tales como:

- Creador.
- Aprobador.
- Nombre de la Empresa.
- Fecha de Creacion.
- Entre otros.

### Visualizacion de Documento Online

Asi tambien se puede ver el documento desde la aplicacion web.

### Edicion de Documento Rutinario

Asi como en todos los listados, se puede editar la informacion asociada a un documento rutinario desde el listado.


## Creacion de Documento Rutinario

Para poder crear un documento rutinario debe realizar lo siguiente:

*	Acceder al listado de documentos rutinarios ([Ver Listado de Documentos Rutinarios](../admin/routineDocument.md#listado-de-documentos-rutinarios "Ver Listado de Documentos Rutinarios")).
*	Acceder al enlace debajo del listado, con el texto "_Crear Documento_".