# Gestion de Usuarios

Cada usuario con el role de administrador de la empresa puede gestionar los usuarios de la misma. El o los usuarios con dicho role, podran realizar las siguientes acciones: 

## Listado de Usuarios

En el listado de usuarios se podra visualizar a todos los usuarios de la empresa. En la columna final se podran visualizar las acciones disponibles sobre cada uno de ellos, a saber:


### Desactivar/Activar Usuario

Se podra desactivar y/o activar un usuario para que no pueda acceder a la aplicacion. 

### Visualizar Perfil de Usuario

Se puede ver el perfil del usuario tal cual se muestra el perfil del usuario logueado (ver [Perfil de Usuario](../myInfo.md#mi-perfil "Ver Perfil de Usuario")).


### Edicion de Usuario

Mediante la edicion de un usuario, se podra modificar los atributos de cada uno de ellos.

### Ausencia de Usuarios

En esta opcion se pueden observar todas las ausencias del usuario, con los detalles de cada una de ellas.

### Conexiones Moviles de Usuarios

Se puede visualizar la conexion movil actual (en caso de tenerla) del usuario seleccionado, pudiendo ademas poder desloguearlo de la aplicacion movil. De esta manera el usuario no podra utilizar la aplicacion movil.